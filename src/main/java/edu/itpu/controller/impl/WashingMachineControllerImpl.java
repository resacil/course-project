package edu.itpu.controller.impl;

import edu.itpu.controller.WashingMachineController;
import edu.itpu.entity.Fridge;
import edu.itpu.entity.WashingMachine;
import edu.itpu.service.FridgeService;
import edu.itpu.service.WashingMachineService;

import java.util.List;
import java.util.Set;

public class WashingMachineControllerImpl implements WashingMachineController {
    private final WashingMachineService washingmachineService;

    public WashingMachineControllerImpl(WashingMachineService washingmachineService) {
        this.washingmachineService = washingmachineService;
    }

    @Override
    public List<WashingMachine> getAll() {
        return washingmachineService.getAll();
    }

    @Override
    public Set<String> getBrandNames() {
        return washingmachineService.getBrandNames();
    }

    @Override
    public List<WashingMachine> getAllByBrand(String brand) {
        List<WashingMachine> allByBrand;
        allByBrand = washingmachineService.getAllByBrand(brand);
        return allByBrand;
    }

    @Override
    public Set<String> getAllCategoryNames() {
        return washingmachineService.getAllCategoryNames();
    }

    @Override
    public List<WashingMachine> getAllByCategory(String category) {
        return washingmachineService.getAllByCategory(category);
    }

    @Override
    public List<WashingMachine> getAllByMinPrice(double inputMinPrice) {
        return washingmachineService.getAllByMinPrice(inputMinPrice);
    }

    @Override
    public List<WashingMachine> getAllByMaxPrice(double inputMaxPrice) {
        return washingmachineService.getAllByMaxPrice(inputMaxPrice);
    }
}
