package edu.itpu.controller.impl;

import edu.itpu.controller.FridgeController;
import edu.itpu.entity.Fridge;
import edu.itpu.service.FridgeService;

import java.util.List;
import java.util.Set;

public class FridgeControllerImpl implements FridgeController {
    private FridgeService fridgeService;

    public FridgeControllerImpl(FridgeService fridgeService) {
        this.fridgeService = fridgeService;
    }

    @Override
    public List<Fridge> getAll() {
        return fridgeService.getAll();
    }

    @Override
    public Set<String> getBrandNames() {
        return fridgeService.getBrandNames();
    }

    @Override
    public List<Fridge> getAllByBrand(String brand) {
        return fridgeService.getAllByBrand(brand);
    }

    @Override
    public Set<String> getAllCategoryNames() {
        return fridgeService.getAllCategoryNames();
    }

    @Override
    public List<Fridge> getAllByCategory(String category) {
        return fridgeService.getAllByCategory(category);
    }

    @Override
    public List<Fridge> getAllByMinPrice(double inputMinPrice) {
        return fridgeService.getAllByMinPrice(inputMinPrice);
    }

    @Override
    public List<Fridge> getAllByMaxPrice(double inputMaxPrice) {
        return fridgeService.getAllByMaxPrice(inputMaxPrice);
    }
}
