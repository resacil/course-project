package edu.itpu.controller.impl;

import edu.itpu.controller.PhoneController;
import edu.itpu.entity.Fridge;
import edu.itpu.entity.Phone;
import edu.itpu.service.FridgeService;
import edu.itpu.service.PhoneService;

import java.util.List;
import java.util.Set;

public class PhoneControllerImpl implements PhoneController {

    private final PhoneService phoneService;

    public PhoneControllerImpl(PhoneService phoneService) {
        this.phoneService = phoneService;
    }

    @Override
    public List<Phone> getAll() {
        return phoneService.getAll();
    }

    @Override
    public Set<String> getBrandNames() {
        return phoneService.getBrandNames();
    }

    @Override
    public List<Phone> getAllByBrand(String brand) {
        return phoneService.getAllByBrand(brand);
    }

    @Override
    public Set<String> getAllCategoryNames() {
        return phoneService.getAllCategoryNames();
    }

    @Override
    public List<Phone> getAllByCategory(String category) {
        return phoneService.getAllByCategory(category);
    }

    @Override
    public List<Phone> getAllByMinPrice(double inputMinPrice) {
        return phoneService.getAllByMinPrice(inputMinPrice);
    }

    @Override
    public List<Phone> getAllByMaxPrice(double inputMaxPrice) {
        return phoneService.getAllByMaxPrice(inputMaxPrice);
    }
}
