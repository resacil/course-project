package edu.itpu.controller;

import edu.itpu.entity.Fridge;
import edu.itpu.entity.Phone;

import java.util.List;
import java.util.Set;

public interface PhoneController {
    List<Phone> getAll();

    Set<String> getBrandNames();

    List<Phone> getAllByBrand(String brand);

    Set<String> getAllCategoryNames();

    List<Phone> getAllByCategory(String category);

    List<Phone> getAllByMinPrice(double inputMinPrice);

    List<Phone> getAllByMaxPrice(double inputMaxPrice);
}
