package edu.itpu.show;

import edu.itpu.controller.FridgeController;
import edu.itpu.controller.PhoneController;
import edu.itpu.controller.WashingMachineController;
import edu.itpu.controller.impl.FridgeControllerImpl;
import edu.itpu.controller.impl.PhoneControllerImpl;
import edu.itpu.controller.impl.WashingMachineControllerImpl;
import edu.itpu.dao.FridgeDao;
import edu.itpu.dao.PhoneDao;
import edu.itpu.dao.WashingMachineDao;
import edu.itpu.dao.impl.FridgeDaoImpl;
import edu.itpu.dao.impl.PhoneDaoImpl;
import edu.itpu.dao.impl.WashingMachineDaoImpl;
import edu.itpu.service.FridgeService;
import edu.itpu.service.PhoneService;
import edu.itpu.service.WashingMachineService;
import edu.itpu.service.impl.FridgeServiceImpl;
import edu.itpu.service.impl.PhoneServiceImpl;
import edu.itpu.service.impl.WashingMachineServiceImpl;

import java.io.IOException;
import java.util.Scanner;

public class Run {
    public static void main() throws IOException {
        System.out.println("" +
                "Developer: Ibrohim Mirzamuhammedov, Ibrohim_Mirzamuhammedov@student.itpu.uz\n" +
                "Project name: Course Project. Household warehouse\n" +
                "Creation date: May , 2023\n" +
                "Version: v1\n\n");

        while (true) {
            FridgeDao fridgeDao = new FridgeDaoImpl();
            FridgeService fridgeService = new FridgeServiceImpl(fridgeDao);
            FridgeController fridgeController = new FridgeControllerImpl(fridgeService);

            PhoneDao phoneDao = new PhoneDaoImpl();
            PhoneService phoneService = new PhoneServiceImpl(phoneDao);
            PhoneController phoneController = new PhoneControllerImpl(phoneService);

            WashingMachineDao washingMachineDao = new WashingMachineDaoImpl();
            WashingMachineService washingMachineService = new WashingMachineServiceImpl(washingMachineDao);
            WashingMachineController washingMachineController = new WashingMachineControllerImpl(washingMachineService);

            System.out.println("""
                    My app has these commands:
                    SEARCHING FRIDGE
                    SEARCHING WASHING-MACHINE
                    SEARCHING PHONE
                    GET ALL FRIDGE
                    GET ALL WASHING-MACHINE
                    GET ALL PHONE
                    STOP
                    """);
            Scanner scanner = new Scanner(System.in);
            String inputStr = scanner.nextLine();


            if (inputStr.equalsIgnoreCase("SEARCHING FRIDGE")) {
                System.out.println("PRICE\nBRAND\nCATEGORY");
                String inputSearch = scanner.nextLine();
                if (inputSearch.equalsIgnoreCase("PRICE")) {
                    System.out.println("Please choose:\nMin price(You'll get households from this price)" +
                            "\nMax price(You'll get households up to this price)");
                    String inputPrice = scanner.nextLine();
                    if (inputPrice.equalsIgnoreCase("min price")) {
                        System.out.println("Please enter min price");
                        double inputMinPrice = scanner.nextDouble();
                        System.out.println(fridgeController.getAllByMinPrice(inputMinPrice));


                    } else if (inputPrice.equalsIgnoreCase("max price")) {
                        System.out.println("Please enter max price");
                        double inputMaxPrice = scanner.nextDouble();
                        System.out.println(fridgeController.getAllByMaxPrice(inputMaxPrice));

                    } else {
                        System.out.println("You wrote incorrect values");
                    }


                } else if (inputSearch.equalsIgnoreCase("BRAND")) {
                    System.out.println(fridgeController.getBrandNames());
                    String brandName = scanner.nextLine();
                    System.out.println(fridgeController.getAllByBrand(brandName));
                } else if (inputSearch.equalsIgnoreCase("CATEGORY")) {
                    System.out.println(fridgeController.getAllCategoryNames());
                    String categoryName = scanner.nextLine();
                    System.out.println(fridgeController.getAllByCategory(categoryName));
                } else {
                    System.err.println("You want to search by  not existing type");
                }

            }

            else if (inputStr.equalsIgnoreCase("SEARCHING WASHING-MACHINE")) {
                System.out.println("PRICE\nBRAND\nCATEGORY");
                String inputSearch = scanner.nextLine();
                if (inputSearch.equalsIgnoreCase("PRICE")) {
                    System.out.println("Please choose:\nMin price(You'll get households from this price)" +
                            "\nMax price(You'll get households up to this price)");
                    String inputPrice = scanner.nextLine();
                    if (inputPrice.equalsIgnoreCase("min price")) {
                        System.out.println("Please enter min price");
                        double inputMinPrice = scanner.nextDouble();
                        System.out.println(washingMachineController.getAllByMinPrice(inputMinPrice));


                    } else if (inputPrice.equalsIgnoreCase("max price")) {
                        System.out.println("Please enter max price");
                        double inputMaxPrice = scanner.nextDouble();
                        System.out.println(washingMachineController.getAllByMaxPrice(inputMaxPrice));

                    } else {
                        System.out.println("You wrote incorrect values");
                    }


                } else if (inputSearch.equalsIgnoreCase("BRAND")) {
                    System.out.println(washingMachineController.getBrandNames());
                    String brandName = scanner.nextLine();
                    System.out.println(washingMachineController.getAllByBrand(brandName));
                } else if (inputSearch.equalsIgnoreCase("CATEGORY")) {
                    System.out.println(washingMachineController.getAllCategoryNames());
                    String categoryName = scanner.nextLine();
                    System.out.println(washingMachineController.getAllByCategory(categoryName));
                } else {
                    System.err.println("You want to search by  not existing type");
                }

            }
            else if (inputStr.equalsIgnoreCase("SEARCHING PHONE")) {
                System.out.println("PRICE\nBRAND\nCATEGORY");
                String inputSearch = scanner.nextLine();
                if (inputSearch.equalsIgnoreCase("PRICE")) {
                    System.out.println("Please choose:\nMin price(You'll get households from this price)" +
                            "\nMax price(You'll get households up to this price)");
                    String inputPrice = scanner.nextLine();
                    if (inputPrice.equalsIgnoreCase("min price")) {
                        System.out.println("Please enter min price");
                        double inputMinPrice = scanner.nextDouble();
                        System.out.println(phoneController.getAllByMinPrice(inputMinPrice));


                    } else if (inputPrice.equalsIgnoreCase("max price")) {
                        System.out.println("Please enter max price");
                        double inputMaxPrice = scanner.nextDouble();
                        System.out.println(phoneController.getAllByMaxPrice(inputMaxPrice));

                    } else {
                        System.out.println("You wrote incorrect values");
                    }


                } else if (inputSearch.equalsIgnoreCase("BRAND")) {
                    System.out.println(phoneController.getBrandNames());
                    String brandName = scanner.nextLine();
                    System.out.println(phoneController.getAllByBrand(brandName));
                } else if (inputSearch.equalsIgnoreCase("CATEGORY")) {
                    System.out.println(phoneController.getAllCategoryNames());
                    String categoryName = scanner.nextLine();
                    System.out.println(phoneController.getAllByCategory(categoryName));
                } else {
                    System.err.println("You want to search by  not existing type");
                }

            }
            else if (inputStr.equalsIgnoreCase("GET ALL FRIDGE")) {
                System.out.println(fridgeController.getAll());
            }
            else if (inputStr.equalsIgnoreCase("GET ALL WASHING-MACHINE")) {
                System.out.println(washingMachineController.getAll());
            }
            else if (inputStr.equalsIgnoreCase("GET ALL PHONE")) {
                System.out.println(phoneController.getAll());
            }
            else if (inputStr.equalsIgnoreCase("STOP")) {
                break;
            }
            else {
                System.out.println("You want to run  by  not existing command");
            }

        }

    }

}
