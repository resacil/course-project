package edu.itpu;

import edu.itpu.show.Run;
import edu.itpu.show.Show;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
//            Show.main();
            Run.main();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
