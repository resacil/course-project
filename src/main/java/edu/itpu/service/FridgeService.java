package edu.itpu.service;

import edu.itpu.entity.Fridge;

import java.util.List;
import java.util.Set;

public interface FridgeService {
    List<Fridge> getAll();

    Set<String> getBrandNames();

    List<Fridge> getAllByBrand(String brand);

    Set<String> getAllCategoryNames();

    List<Fridge> getAllByCategory(String category);

    List<Fridge> getAllByMinPrice(double inputMinPrice);

    List<Fridge> getAllByMaxPrice(double inputMaxPrice);
}
