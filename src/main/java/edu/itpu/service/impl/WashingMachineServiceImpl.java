package edu.itpu.service.impl;

import edu.itpu.dao.FridgeDao;
import edu.itpu.dao.WashingMachineDao;
import edu.itpu.entity.Fridge;
import edu.itpu.entity.WashingMachine;
import edu.itpu.service.WashingMachineService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WashingMachineServiceImpl implements WashingMachineService {
    private final WashingMachineDao washingMachineDao;

    public WashingMachineServiceImpl(WashingMachineDao washingMachineDao) {
        this.washingMachineDao = washingMachineDao;
    }

    @Override
    public List<WashingMachine> getAll() {
        return washingMachineDao.getWashingMachineList();
    }

    @Override
    public Set<String> getBrandNames() {
        List<WashingMachine> washingMachineList = washingMachineDao.getWashingMachineList();
        Set<String> strings = new HashSet<>();
        for (WashingMachine washingMachine : washingMachineList) {
            strings.add(washingMachine.getBrand());
        }
        return strings;
    }

    @Override
    public List<WashingMachine> getAllByBrand(String brand) {
        List<WashingMachine> washingMachineList = washingMachineDao.getWashingMachineList();
        List<WashingMachine> washingMachines = new ArrayList<>();
        for (WashingMachine washingMachine : washingMachineList) {
            if (washingMachine.getBrand().equalsIgnoreCase(brand)){
                washingMachines.add(washingMachine);
            }
        }
        return washingMachines;
    }

    @Override
    public Set<String> getAllCategoryNames() {
        List<WashingMachine> washingMachines = washingMachineDao.getWashingMachineList();
        Set<String> strings = new HashSet<>();
        for (WashingMachine washingMachine : washingMachines) {
            strings.add(washingMachine.getCategory());
        }
        return strings;
    }

    @Override
    public List<WashingMachine> getAllByCategory(String category) {
        List<WashingMachine> washingMachines = washingMachineDao.getWashingMachineList();
        List<WashingMachine> washingMachineList = new ArrayList<>();
        for (WashingMachine washingMachine : washingMachines) {
            if (washingMachine.getCategory().equalsIgnoreCase(category)){
                washingMachineList.add(washingMachine);
            }
        }

        return washingMachineList;
    }

    @Override
    public List<WashingMachine> getAllByMinPrice(double inputMinPrice) {
        List<WashingMachine> washingMachineList = washingMachineDao.getWashingMachineList();
        List<WashingMachine> washingMachines = new ArrayList<>();
        for (WashingMachine washingMachine : washingMachineList) {
            if (washingMachine.getPrice()>=inputMinPrice){
                washingMachines.add(washingMachine);
            }
        }
        return washingMachines;
    }

    @Override
    public List<WashingMachine> getAllByMaxPrice(double inputMaxPrice) {
        List<WashingMachine> washingMachineList = washingMachineDao.getWashingMachineList();
        List<WashingMachine> washingmachines = new ArrayList<>();
        for (WashingMachine washingmachine : washingMachineList) {
            if (washingmachine.getPrice()<=inputMaxPrice){
                washingmachines.add(washingmachine);
            }
        }
        return washingmachines;
    }
}
