package edu.itpu.service.impl;

import edu.itpu.dao.FridgeDao;
import edu.itpu.entity.Fridge;
import edu.itpu.service.FridgeService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FridgeServiceImpl implements FridgeService {
    private final FridgeDao fridgeDao;

    public FridgeServiceImpl(FridgeDao fridgeDao) {
        this.fridgeDao = fridgeDao;
    }

    @Override
    public List<Fridge> getAll() {
        return fridgeDao.getFridgeList();
    }

    @Override
    public Set<String> getBrandNames() {
        List<Fridge> fridgeList = fridgeDao.getFridgeList();
        Set<String> strings = new HashSet<>();
        for (Fridge fridge : fridgeList) {
            strings.add(fridge.getBrand());
        }
        return strings;
    }

    @Override
    public List<Fridge> getAllByBrand(String brand) {
        List<Fridge> fridgeList = fridgeDao.getFridgeList();
        List<Fridge> fridges = new ArrayList<>();
        for (Fridge fridge : fridgeList) {
            if (fridge.getBrand().equalsIgnoreCase(brand)){
                fridges.add(fridge);
            }
        }
        return fridges;
    }

    @Override
    public Set<String> getAllCategoryNames() {
        List<Fridge> fridgeList = fridgeDao.getFridgeList();
        Set<String> strings = new HashSet<>();
        for (Fridge fridge : fridgeList) {
            strings.add(fridge.getCategory());
        }
        return strings;
    }

    @Override
    public List<Fridge> getAllByCategory(String category) {
        List<Fridge> fridgeList = fridgeDao.getFridgeList();
        List<Fridge> fridges = new ArrayList<>();
        for (Fridge fridge : fridgeList) {
            if (fridge.getCategory().equalsIgnoreCase(category)){
                fridges.add(fridge);
            }
        }
        return fridges;
    }

    @Override
    public List<Fridge> getAllByMinPrice(double inputMinPrice) {
        List<Fridge> fridgeList = fridgeDao.getFridgeList();
        List<Fridge> fridges = new ArrayList<>();
        for (Fridge fridge : fridgeList) {
            if (fridge.getPrice()>=inputMinPrice){
                fridges.add(fridge);
            }
        }
        return fridges;
    }

    @Override
    public List<Fridge> getAllByMaxPrice(double inputMaxPrice) {
        List<Fridge> fridgeList = fridgeDao.getFridgeList();
        List<Fridge> fridges = new ArrayList<>();
        for (Fridge fridge : fridgeList) {
            if (fridge.getPrice()<=inputMaxPrice){
                fridges.add(fridge);
            }
        }
        return fridges;
    }
}
