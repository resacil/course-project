package edu.itpu.service.impl;

import edu.itpu.dao.PhoneDao;
import edu.itpu.entity.Phone;
import edu.itpu.service.PhoneService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PhoneServiceImpl implements PhoneService {
    private final PhoneDao phoneDao;

    public PhoneServiceImpl(PhoneDao phoneDao) {
        this.phoneDao = phoneDao;
    }

    @Override
    public List<Phone> getAll() {
        return phoneDao.getPhoneList();
    }

    @Override
    public Set<String> getBrandNames() {
        List<Phone> phoneList = phoneDao.getPhoneList();
        Set<String> strings = new HashSet<>();
        for (Phone phone : phoneList) {
            strings.add(phone.getBrand());
        }
        return strings;
    }

    @Override
    public List<Phone> getAllByBrand(String brand) {
        List<Phone> phoneList = phoneDao.getPhoneList();
        List<Phone> phoneList1 = new ArrayList<>();
        for (Phone phone : phoneList) {
            if (phone.getBrand().equalsIgnoreCase(brand)){
                phoneList1.add(phone);
            }
        }
        return phoneList1;
    }

    @Override
    public Set<String> getAllCategoryNames() {
        List<Phone> phoneList = phoneDao.getPhoneList();
        Set<String> strings = new HashSet<>();
        for (Phone phone : phoneList) {
            strings.add(phone.getCategory());
        }
        return strings;
    }

    @Override
    public List<Phone> getAllByCategory(String category) {
        List<Phone> phoneList = phoneDao.getPhoneList();
        List<Phone> phones = new ArrayList<>();
        for (Phone phone : phoneList) {
            if (phone.getCategory().equalsIgnoreCase(category)){
                phones.add(phone);
            }
        }
        return phones;
    }

    @Override
    public List<Phone> getAllByMinPrice(double inputMinPrice) {
        List<Phone> phoneList = phoneDao.getPhoneList();
        List<Phone> phones  = new ArrayList<>();
        for (Phone phone : phoneList) {
            if (phone.getPrice()>=inputMinPrice){
                phones.add(phone);
            }
        }
        return phones;
    }

    @Override
    public List<Phone> getAllByMaxPrice(double inputMaxPrice) {
        List<Phone> phoneList = phoneDao.getPhoneList();
        List<Phone> phones = new ArrayList<>();
        for (Phone phone : phoneList) {
            if (phone.getPrice()<=inputMaxPrice){
                phones.add(phone);
            }
        }
        return phones;
    }
}
