package edu.itpu.service;

import edu.itpu.entity.Phone;

import java.util.List;
import java.util.Set;

public interface PhoneService {
    List<Phone> getAll();

    Set<String> getBrandNames();

    List<Phone> getAllByBrand(String brand);

    Set<String> getAllCategoryNames();

    List<Phone> getAllByCategory(String category);

    List<Phone> getAllByMinPrice(double minPrice);

    List<Phone> getAllByMaxPrice(double inputMaxPrice);

}
