package edu.itpu.service;

import edu.itpu.entity.Fridge;
import edu.itpu.entity.WashingMachine;

import java.util.List;
import java.util.Set;

public interface WashingMachineService {
    List<WashingMachine> getAll();

    Set<String> getBrandNames();

    List<WashingMachine> getAllByBrand(String brand);

    Set<String> getAllCategoryNames();

    List<WashingMachine> getAllByCategory(String category);

    List<WashingMachine> getAllByMinPrice(double inputMinPrice);

    List<WashingMachine> getAllByMaxPrice(double inputMaxPrice);

}
