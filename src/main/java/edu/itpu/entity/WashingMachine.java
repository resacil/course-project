package edu.itpu.entity;

import java.util.Objects;

public class WashingMachine extends Household{
     private Integer loadWeight;

     public WashingMachine() {
     }

     public WashingMachine(Integer loadWeight) {
          this.loadWeight = loadWeight;
     }

     public WashingMachine(Integer id, Double price, Integer quantity, String name, String brand, String model, String color, String efficiency, String madeIn, String category, Integer loadWeight) {
          super(id, price, quantity, name, brand, model, color, efficiency, madeIn, category);
          this.loadWeight = loadWeight;
     }

     public Integer getLoadWeight() {
          return loadWeight;
     }

     public void setLoadWeight(Integer loadWeight) {
          this.loadWeight = loadWeight;
     }

     @Override
     public String toString() {
          return "\nWashingMachine{" +
                  "id=" + super.getId() +
                  ", price=" + super.getPrice() +
                  ", quantity=" + super.getQuantity() +
                  ", name='" + super.getName() +
                  ", brand='" + super.getBrand() +
                  ", model='" + super.getModel() +
                  ", color='" + super.getColor() +
                  ", efficiency='" + super.getEfficiency() +
                  ", madeIn='" + super.getMadeIn() +
                  ", category='" + super.getCategory() +
                  ", loadWeight=" + loadWeight +
                  '}';
     }

     @Override
     public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || getClass() != o.getClass()) return false;
          if (!super.equals(o)) return false;
          WashingMachine that = (WashingMachine) o;
          return Objects.equals(loadWeight, that.loadWeight);
     }

     @Override
     public int hashCode() {
          return Objects.hash(super.hashCode(), loadWeight);
     }
}
