package edu.itpu.entity;

import java.util.Objects;

public class Fridge extends Household {
    private Integer litre;
    private String height;

    public Fridge() {
    }

    public Fridge(Integer litre, String height) {
        this.litre = litre;
        this.height = height;
    }

    public Fridge(Integer id, Double price, Integer quantity, String name, String brand, String model, String color, String efficiency, String madeIn, String category, Integer litre, String height) {
        super(id, price, quantity, name, brand, model, color, efficiency, madeIn, category);
        this.litre = litre;
        this.height = height;
    }

    public Integer getLitre() {
        return litre;
    }

    public void setLitre(Integer litre) {
        this.litre = litre;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "\nFridge{" +
                "id=" + super.getId() +
                ", price=" + super.getPrice() +
                ", quantity=" + super.getQuantity() +
                ", name='" + super.getName() +
                ", brand='" + super.getBrand() +
                ", model='" + super.getModel() +
                ", color='" + super.getColor() +
                ", efficiency='" + super.getEfficiency() +
                ", madeIn='" + super.getMadeIn() +
                ", category='" + super.getCategory() +
                ", litre=" + litre +
                ", height='" + height +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Fridge fridge = (Fridge) o;
        return Objects.equals(litre, fridge.litre) && Objects.equals(height, fridge.height);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), litre, height);
    }
}
