package edu.itpu.entity;

import java.util.Objects;

public class Phone extends  Household{
    private String ram;
    private String cpu;

    public Phone() {
    }

    public Phone(String ram, String cpu) {
        this.ram = ram;
        this.cpu = cpu;
    }

    public Phone(Integer id, Double price, Integer quantity, String name, String brand, String model, String color, String efficiency, String madeIn, String category, String ram, String cpu) {
        super(id, price, quantity, name, brand, model, color, efficiency, madeIn, category);
        this.ram = ram;
        this.cpu = cpu;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    @Override
    public String toString() {
        return "\nPhone{" +
                "id=" + super.getId() +
                ", price=" + super.getPrice() +
                ", quantity=" + super.getQuantity() +
                ", name='" + super.getName() +
                ", brand='" + super.getBrand() +
                ", model='" + super.getModel() +
                ", color='" + super.getColor() +
                ", efficiency='" + super.getEfficiency() +
                ", madeIn='" + super.getMadeIn() +
                ", category='" + super.getCategory() +
                ", ram='" + ram + '\'' +
                ", cpu='" + cpu + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Phone phone = (Phone) o;
        return Objects.equals(ram, phone.ram) && Objects.equals(cpu, phone.cpu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), ram, cpu);
    }
}
