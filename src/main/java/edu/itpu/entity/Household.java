package edu.itpu.entity;

import java.util.Objects;

public abstract class Household {
    private Integer id;
    private Double price;
    private Integer quantity;
    private String name;
    private String brand;
    private String model;
    private String color;
    private String efficiency;
    private String madeIn;
    private String category;

    public Household() {
    }

    public Household(Integer id, Double price, Integer quantity, String name, String brand, String model, String color, String efficiency, String madeIn, String category) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
        this.name = name;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.efficiency = efficiency;
        this.madeIn = madeIn;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(String efficiency) {
        this.efficiency = efficiency;
    }

    public String getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(String madeIn) {
        this.madeIn = madeIn;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Household{" +
                "id=" + id +
                ", price=" + price +
                ", quantity=" + quantity +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", efficiency='" + efficiency + '\'' +
                ", madeIn='" + madeIn + '\'' +
                ", category='" + category + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Household household = (Household) o;
        return Objects.equals(id, household.id) && Objects.equals(price, household.price) && Objects.equals(quantity, household.quantity) && Objects.equals(name, household.name) && Objects.equals(brand, household.brand) && Objects.equals(model, household.model) && Objects.equals(color, household.color) && Objects.equals(efficiency, household.efficiency) && Objects.equals(madeIn, household.madeIn) && Objects.equals(category, household.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price, quantity, name, brand, model, color, efficiency, madeIn, category);
    }
}
