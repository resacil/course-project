package edu.itpu.dao;

import edu.itpu.entity.Fridge;

import java.util.List;

public interface FridgeDao {
    List<String[]> getDataFromCSVFile();
    List<Fridge> getFridgeList();
}
