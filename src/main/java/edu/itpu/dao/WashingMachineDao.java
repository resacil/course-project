package edu.itpu.dao;

import edu.itpu.entity.Phone;
import edu.itpu.entity.WashingMachine;

import java.util.List;

public interface WashingMachineDao {
    List<String[]> getDataFromCSVFile();
    List<WashingMachine> getWashingMachineList();
}
