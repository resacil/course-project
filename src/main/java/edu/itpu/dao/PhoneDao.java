package edu.itpu.dao;

import edu.itpu.entity.Fridge;
import edu.itpu.entity.Phone;

import java.util.List;

public interface PhoneDao {
    List<String[]> getDataFromCSVFile();
    List<Phone> getPhoneList();

}
