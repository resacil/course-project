package edu.itpu.dao.impl;

import edu.itpu.dao.FridgeDao;
import edu.itpu.entity.Fridge;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FridgeDaoImpl implements FridgeDao {
    @Override
    public List<String[]> getDataFromCSVFile() {
        List<String[]> listOfHouseholds = new ArrayList<>();
        try (
                BufferedReader bread = new BufferedReader(new FileReader("src/main/resources/fridge.csv"))) {
            String line = "";
            while ((line = bread.readLine()) != null) {
                String[] fromStringToArray = line.split(",");
                listOfHouseholds.add(fromStringToArray);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return listOfHouseholds;
    }

    @Override
    public List<Fridge> getFridgeList() {
        List<String[]> dataFromCSVFile = getDataFromCSVFile();
        List<Fridge> fridgeList = new ArrayList<>();
        for (String[] obj : dataFromCSVFile) {
            if (obj[0].equalsIgnoreCase("id")) {
                continue;
            }
            Fridge fridge = new Fridge();
            fridge.setId(Integer.valueOf(obj[0]));
            fridge.setPrice(Double.parseDouble(obj[1]));
            fridge.setQuantity(Integer.valueOf(obj[2]));
            fridge.setName(obj[3]);
            fridge.setBrand(obj[4]);
            fridge.setModel(obj[5]);
            fridge.setColor(obj[6]);
            fridge.setEfficiency(obj[7]);
            fridge.setMadeIn(obj[8]);
            fridge.setCategory(obj[9]);
            fridgeList.add(fridge);
        }
        return fridgeList;
    }
}
