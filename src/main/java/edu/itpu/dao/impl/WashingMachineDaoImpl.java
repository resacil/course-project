package edu.itpu.dao.impl;

import edu.itpu.dao.WashingMachineDao;
import edu.itpu.entity.WashingMachine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WashingMachineDaoImpl implements WashingMachineDao {
    @Override
    public List<String[]> getDataFromCSVFile() {
        List<String[]> listOfHouseholds = new ArrayList<>();
        try (
                BufferedReader bread = new BufferedReader(new FileReader("src/main/resources/washingmachine.csv"))) {
            String line = "";
            while ((line = bread.readLine()) != null) {
                String[] fromStringToArray = line.split(",");
                listOfHouseholds.add(fromStringToArray);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return listOfHouseholds;
    }

    public List<WashingMachine> getWashingMachineList() {
        List<String[]> dataFromCSVFile = getDataFromCSVFile();
        List<WashingMachine> washingMachineList = new ArrayList<>();
        for (String[] obj : dataFromCSVFile) {
            if (obj[0].equalsIgnoreCase("id")) {
                continue;
            }
            WashingMachine washineMachine = new WashingMachine();
            washineMachine.setId(Integer.valueOf(obj[0]));
            washineMachine.setPrice(Double.parseDouble(obj[1]));
            washineMachine.setQuantity(Integer.valueOf(obj[2]));
            washineMachine.setName(obj[3]);
            washineMachine.setBrand(obj[4]);
            washineMachine.setModel(obj[5]);
            washineMachine.setColor(obj[6]);
            washineMachine.setEfficiency(obj[7]);
            washineMachine.setMadeIn(obj[8]);
            washineMachine.setCategory(obj[9]);
            washingMachineList.add(washineMachine);
        }
        return washingMachineList;
    }
}
