package edu.itpu.dao.impl;

import edu.itpu.dao.PhoneDao;
import edu.itpu.entity.Fridge;
import edu.itpu.entity.Phone;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PhoneDaoImpl implements PhoneDao {
    @Override
    public List<String[]> getDataFromCSVFile() {
        List<String[]> listOfHouseholds = new ArrayList<>();
        try (
                BufferedReader bread = new BufferedReader(new FileReader("src/main/resources/phone.csv"))) {
            String line = "";
            while ((line = bread.readLine()) != null) {
                String[] fromStringToArray = line.split(",");
                listOfHouseholds.add(fromStringToArray);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return listOfHouseholds;
    }

    @Override
    public List<Phone> getPhoneList() {
        List<String[]> dataFromCSVFile = getDataFromCSVFile();
        List<Phone> phoneList = new ArrayList<>();
        for (String[] obj : dataFromCSVFile) {
            if (obj[0].equalsIgnoreCase("id")) {
                continue;
            }
            Phone phone = new Phone();
            phone.setId(Integer.valueOf(obj[0]));
            phone.setPrice(Double.parseDouble(obj[1]));
            phone.setQuantity(Integer.valueOf(obj[2]));
            phone.setName(obj[3]);
            phone.setBrand(obj[4]);
            phone.setModel(obj[5]);
            phone.setColor(obj[6]);
            phone.setEfficiency(obj[7]);
            phone.setMadeIn(obj[8]);
            phone.setCategory(obj[9]);
            phoneList.add(phone);
        }
        return phoneList;
    }
}
